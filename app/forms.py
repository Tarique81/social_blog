from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class SignupForm(UserCreationForm):
    username=forms.CharField(max_length=100)
    email=forms.EmailField(required=False)
    password1=forms.CharField(max_length=100)
    password2=forms.CharField(max_length=100)
    class Meta:
        model= User
        fields=('username','password1','password2','email',)

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            user = User.objects.exclude(pk=self.instance.pk).get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(u'Username "%s" is already in use.' % username)

class ProfileForm(forms.ModelForm):
    bio=forms.CharField(max_length=30,required=False)
    birth_date=forms.DateField(required=False)
    class Meta:
        model= User
        fields=('first_name','last_name','email','bio','birth_date',)
        # exclude=('password','password1')

