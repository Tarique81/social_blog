from django.conf.urls import url
from app.views import signup, article_home,article_create,article_detail

urlpatterns=[
    url(r'^signup/$',signup,name='signup'),
    url(r'^$',article_home,name='article'),
    url(r'^create/$',article_create,name='article_create'),
    url(r'^detail/(?P<pk>\d+)/$',article_detail,name='article_detail')
]