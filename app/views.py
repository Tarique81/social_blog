# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import login, authenticate
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from app.tokens import account_activation_token
from django.contrib.sites.shortcuts import get_current_site
from .forms import SignupForm, ProfileForm
from django.contrib.auth.models import User
from article.models import Article
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# Create your views here.


def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.save()
            authenticate_user = authenticate(username=form.cleaned_data.get('username'),
                                             password=form.cleaned_data.get('password1'))
            login(request, authenticate_user)
            # current_site = get_current_site(request)
            # subject = 'Activate Your MySite Account'
            # message = render_to_string('account_activation_email.html', {
            #     'user': user,
            #     'domain': current_site.domain,
            #     'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            #     'token': account_activation_token.make_token(user),
            # })
            # user.email_user(subject, message)
            return redirect('/')
    else:
        form = SignupForm()
    return render(request, 'authentication/signup.html', {'form': form})


def update_profile(request):
    user = request.user
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            user.first_name = form.cleaned_data.get('first_name')
            user.last_name = form.cleaned_data.get('last_name')
            user.email = form.cleaned_data.get('email')
            user.profile.bio = form.cleaned_data.get('bio')
            user.profile.birth_date = form.cleaned_data.get('birth_date')
            user.save()
            return redirect('profile')
    else:
        form = ProfileForm(instance=user, initial={
            'bio': user.profile.bio,
            'birth_date': user.profile.birth_date,
        })
    return render(request, 'profile/update_profile.html', {'form': form})


def account_activation_sent(request):
    return render(request, 'account_activation_sent.html')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('home')
    else:
        return render(request, 'account_activation_invalid.html')


def home(request):
    article_list = Article.objects.all().distinct().order_by('-created_at')
    paginator = Paginator(article_list, 2)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        articles = paginator.page(paginator.num_pages)
    recent_articles = Article.objects.all().order_by('-created_at')[:3]

    return render(request, 'home.html', {
        'articles': articles,
        'recent_articles': recent_articles
    })
