from django import forms
from .models import Article,Comment


class ArticleForm(forms.Form):
    title=forms.CharField(max_length=100)
    description=forms.CharField(widget=forms.Textarea)
class CommentForm(forms.Form):
    comment= forms.CharField(max_length=200,required=True)