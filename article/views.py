# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect, get_object_or_404
from .models import Article, Comment
from .forms import ArticleForm, CommentForm
# Create your views here.

def article_home(request):
    articles = Article.objects.filter(user=request.user)
    return render(request, 'article/article_home.html', {
        'articles': articles
    })
def article_create(request):
    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            user = request.user
            title = form.cleaned_data.get('title')
            description = form.cleaned_data.get('description')
            article = Article(user=user, title=title, description=description)
            article.save()
            return redirect('article')
    else:
        form = ArticleForm
    return render(request, 'article/article_create.html', {'form': form})


def article_detail(request, pk):
    article = get_object_or_404(Article, pk=pk)
    return render(request, 'article/article_detail.html', {'article': article})
def comment_create(request,pk):
    articles=Article.objects.all().distinct()
    if request.method== 'POST':
        form= CommentForm(request.POST)
        if form.is_valid():
            comment=Comment(user=request.user,article_id=pk,comment=form.cleaned_data.get('comment'))
            comment.save()

            return redirect('home')
    else:
        form=CommentForm
    return render(request,'home.html',{
        'form':form,
        'articles':articles
    })
