from django.conf.urls import url
from .views import article_home,article_create,article_detail,comment_create
urlpatterns=[
    url(r'^$',article_home,name='article'),
    url(r'^create/$',article_create,name='article_create'),
    url(r'^detail/(?P<pk>\d+)/$',article_detail,name='article_detail'),
    url(r'^comment/create/(?P<pk>\d+)/$', comment_create, name='comment_create'),

]