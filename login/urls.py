from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView
from app import views
from room.views import index


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^article/', include('article.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^about/$', TemplateView.as_view(template_name='about.html'), name='about'),
    url(r'^profile/$', TemplateView.as_view(template_name='profile/view_profile.html'), name='profile'),
    url(r'^login/$', auth_views.login, {'template_name': 'authentication/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'authentication/logged_out.html'}, name='logout'),
    url(r'^signup/$',views.signup,name='signup'),
    url(r'^chat/$',index,name='chat'),
    url(r'^update/$',views.update_profile,name='update'),
    url(r'^account_activation_sent/$', views.account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
]
